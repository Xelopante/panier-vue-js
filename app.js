const TOKEN = 'yann.hittin8@etu.univ-lorraine.fr';
const API = 'https://tools.sopress.net/iut/panier/api/';

var app = new Vue({
    el :'#wrapper',
    created() {
        this.chargerProduits();
        this.chargerPanier();
    },
    data : {
        liste_produits : [],
        panier : [],
        total_panier : 0
    },
    methods : {
        chargerProduits() {
            fetch(`${API}/products?token=${TOKEN}`).then((response) => {
                return response.json();
            }).then((produits) => {
                this.liste_produits = produits;
            });
        },
        chargerPanier() {
            fetch(`${API}/cart?token=${TOKEN}`).then((response) => {
                return response.json();
            }).then((panier) => {
                this.panier = panier;
                this.calculerTotalPanier;
            });
        },
        addProduitPanier(id_produit) {
            fetch(`${API}/cart/${id_produit}?token=${TOKEN}`,{method: "POST"}).then((response) => {
                return response.json();
            }).then((produits_panier) => {
                this.panier = produits_panier;
                this.calculerTotalPanier;
            });
        },
        viderPanier() {
            fetch(`${API}/cart?token=${TOKEN}`,{method: "DELETE"}).then((response) => {
                return response.json();
            }).then((produits_panier) => {
                this.panier = produits_panier;
                this.calculerTotalPanier;
            });
        },
        supprimerProduitPanier(id_produit) {
            fetch(`${API}/cart/${id_produit}?token=${TOKEN}`,{method: "DELETE"}).then((response) => {
                return response.json();
            }).then((produits_panier) => {
                this.panier = produits_panier;
                this.calculerTotalPanier;
            });
        },
        commanderPanier() {
            let nb_produits_commandes = 0;

            this.panier.forEach(produit => {
                fetch(`${API}/cart/${produit.id}/buy?token=${TOKEN}`,{method: "PUT"}).then((response) => {
                    return response.json();
                }).then(() => {
                    nb_produits_commandes += 1;
                    this.alertPanier(nb_produits_commandes);
                });
            })      
        },
        alertPanier(nb_produits_commandes) {

            if(nb_produits_commandes == this.panier.length){
                alert("Tous les produits sont commandés !");
            }
        },
    },
    computed : {
        calculerTotalPanier() {
            this.total_panier = 0;
            this.panier.forEach(produit => {
                this.total_panier += produit.prix;
            });
        }
    }
});