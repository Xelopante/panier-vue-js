'use strict';

const TOKEN = 'yann.hittin8@etu.univ-lorraine.fr';
const API = 'https://tools.sopress.net/iut/panier/api/';

(function() {
    //Déclaration du namespace
    let panier = (function() {
        return {
            modules : {}
        }
    })();

    //Actions
    panier.modules.actions = (function() {
        let page_actuelle = 1;
        let filtre_actuel = null;
        let ordre_actuel = null;

        return {
            /*
            Insère les boutons de filtrea près les produits et avant le panier
            */
            construireFiltre: () => {
                let filtre = "<div id='filtre'><button data-filtre='name'>Trier par Nom</button><button data-filtre='price'>Trier par Prix</button></div>";

                $(filtre).insertAfter($('#products'));
            },
            construirePagination: () => {

                let pagination = "<div id='pagination'>";

                //if(page_actuelle > 1) {
                pagination += "<button data-position='moins' data-page='"+page_actuelle+"'><</button>";
                //}

                pagination += "<button data-position='plus' data-page='"+page_actuelle+"'>></button></div>";

                $(pagination).insertBefore($('#wrapper'));

                /*$("#pagination button").each(function() {
                    $(this).click(function() {
                        if($(this).data("page")) {
                            panier.modules.actions.changerPage($(this).data("page") + 1);
                        }changerPage

                        $('#pagination').remove();
                        panier.modules.actions.construirePagination();
                        panier.modules.actions.TrierProduit();
                    });
                });*/
            },
            /*
            Récupère des objets produits
            Construit un fragment HTMl avec les informations de chacun et le retourne
            */
            construireListeproduits: (lesProduit) => {
                let ProduitsHTML = "";

                $.each( lesProduit, function( key, value ) {
                    ProduitsHTML += "<div class='produit'><h3>"+ value.nom +"</h3><br /><img src='" + value.photo
                    + "' /><p>" + value.description + "</p><p>"+value.prix+" &euro;</p><button data-id='"+value.id+"'>Ajouter au panier</button></div>";
                });
                $("#products").html(ProduitsHTML);
            },
            /*
            Avec l'id d'un produit en paramètre
            Effectue une requête AJAX POST pour l'ajouter au panier
            */
            addProduitPanier: (idProduit) => {
                fetch(`${API}/cart/${idProduit}?token=${TOKEN}`,{method: "POST"}).then(function (response) {
                    return response.json();
                }).then(function (produitsPanier) {
                    panier.modules.actions.affichagePanier(produitsPanier);
                });
            },
            /*
            Avec l'id d'un produit en paramètre
            Efectue une requête AJAX DELETE pour supprimer le produit du panier
            */
            deleteProduitPanier: (idProduit) => {
                fetch(`${API}/cart/${idProduit}?token=${TOKEN}`,{method: "DELETE"}).then(function (response) {
                    return response.json();
                }).then(function (produitsPanier) {
                    panier.modules.actions.affichagePanier(produitsPanier);
                });
            },
            /*
            Effectue une requête AJAX DELETE pour supprimer
            tous les produits du panier
            */
            EmptyPanier: () => {
                fetch(`${API}/cart?token=${TOKEN}`,{method: "DELETE"}).then(function (response) {
                    return response.json();
                }).then(function (produitsPanier) {
                    panier.modules.actions.affichagePanier(produitsPanier);
                });
            },
            /*
            Avec la liste des produits du panier en paramètres
            Effectue une requête AJAX PUT pour commander les produits du panier
            Une fois commandé, la couleur de fond de sa div dans la panier est verte
            */
            BuyPanier: (produitsPanier) => {
                let count = 0;

                $.each( produitsPanier, function( key, value ) {
                    fetch(`${API}/cart/${value.id}/buy?token=${TOKEN}`,{method: "PUT"}).then(function (response) {
                        return response.json();
                    }).then(function () {

                        $(".produitPanier:eq("+key+")").css("background-color", "green");
                        count += 1;
                        panier.modules.actions.AlertPanier(count);
                    });
                })
            },
            /*
            Avec le nombre de produits du panier commandés en paramètres
            Compare avec le nombre de produits totaux dans le panier
            Si il correspond, tous les produtis sont donc commandés
            On affiche une alerte pour le signaler
            */
            AlertPanier: (count) => {
                let nbProduit = $(".produitPanier").length;

                if(count == nbProduit){
                    alert("Tous les produits sont commandés !");
                }
            },
            /*
            Avec les produits du panier en paramètres
            On construit le fragment HTML qui correspond aux produits dans le panier
            On incrémente le prix total du panier
            On ajoute un handler sur le click du bouton "retirer" de chaque produit
            qui déclenche une fonction qui le supprime du panier
            */
            affichagePanier: (produitsPanier) => {
                let ProduitsHTML = "";
                let PrixT = 0;

                $.each( produitsPanier, function( key, value ) {
                    ProduitsHTML += "<div class='produitPanier'><p>"+ value.nom +" : "+ value.qte + " : " +value.prix +" &euro;</p><button data-id='"+ value.id +"'>Retirer</button></div>";
                    PrixT += value.prix;
                });

                ProduitsHTML += "<p>Prix Total : "+ PrixT +"</p>";
                $("#panier-content").html(ProduitsHTML);

                $(".produitPanier button").click(function(e) {
                    let idProduit = e.target.dataset.id;
                    panier.modules.actions.deleteProduitPanier(idProduit);
                });
            },
            TrierProduit: () => {
                let url = `${API}/products?token=${TOKEN}&page=${page_actuelle}`;
                console.log(page_actuelle);

                if(filtre_actuel != null) {
                    url += `&field=${filtre_actuel}`;
                }

                if(ordre_actuel != null) {
                    url += `&sort=${ordre_actuel}`;
                }

                fetch(url).then(function (response) {
                    return response.json();
                }).then(function (produits) {
                    /*
                    Si on les récupère correctement, on appelle une fonction qui
                    va les afficher dans la page
                    */
                    $("#products").html("");
                    panier.modules.actions.construireListeproduits(produits);

                    /*
                    Ajout d'un handler sur le bouton "Ajouter au panier" de chaque produit
                    */
                    $(".produit button").click(function(e) {
                        let idProduit = e.target.dataset.id;
                        panier.modules.actions.addProduitPanier(idProduit);
                    });
                });
            },
            changerFiltre: (filtre) => {
                filtre_actuel = filtre;
            },
            changerOrdre: (ordre) => {
                ordre_actuel = ordre;
            },
            changerPage: (page) => {
                page_actuelle = page;
            }
        }
    })();

    panier.modules.app = (function() {
        return {
            start() {
                panier.modules.actions.construireFiltre();
                panier.modules.actions.construirePagination();
                
                /*
                Requête AJAX GET qu irécupère tous les produits de l'API
                */
                fetch(`${API}/products?token=${TOKEN}`).then(function (response) {
                    return response.json();
                }).then(function (produits) {
                    /*
                    Si on les récupère correctement, on appelle une fonction qui
                    va les afficher dans la page
                    */
                    panier.modules.actions.construireListeproduits(produits);

                    /*
                    Ajout d'un handler sur le bouton "Ajouter au panier" de chaque produit
                    */
                    $(".produit button").click(function(e) {
                        let idProduit = e.target.dataset.id;
                        panier.modules.actions.addProduitPanier(idProduit);
                    });
                });

                /*
                Requête AJAX GET qui récupère tous les produits du panier de l'utilisateur
                */
                fetch(`${API}/cart?token=${TOKEN}`).then(function (response) {
                    return response.json();
                }).then(function (produitsPanier) {
                    /*
                    si on les récupère correctement, on les affiche dans la page
                    */
                    panier.modules.actions.affichagePanier(produitsPanier);

                    /*
                    Ajout d'un handler sur le bouton "Retirer" de chaque
                    produit du panier
                    */
                    $(".produitPanier button").click(function(e) {
                        let idProduit = e.target.dataset.id;
                        panier.modules.actions.deleteProduitPanier(idProduit);
                    });

                    /*
                    Ajout d'un handler sur le bouton "Vider le panier"
                    */
                    $("#empty").click(function() {
                        panier.modules.actions.EmptyPanier();
                    });

                    /*
                    Ajout d'un handler sur le bouton "Commander"
                    */
                    $("#buy").click(function(e) {
                        panier.modules.actions.BuyPanier(produitsPanier);
                    });
                });
                $("#filtre button").each(function() {
                    $(this).click(function() {
                        if($(this).data("filtre")) {
                            panier.modules.actions.changerFiltre($(this).data("filtre"));
                        }

                        panier.modules.actions.TrierProduit();
                    });
                });
                $("#pagination button").each(function() {
                    $(this).click(function() {
                        console.log($(this));
                        console.log($(this).data("page"));
                        if($(this).data("page")) {
                            if($(this).data("position") == "plus") {
                                panier.modules.actions.changerPage($(this).data("page") + 1);
                                $(this).data("page",$(this).data("page")+1);
                            }
                            else if($(this).data("position") == "moins") {
                                panier.modules.actions.changerPage($(this).data("page") - 1);
                                $(this).data("page",($(this).data("page")-1));
                            }
                        }
                        if($(this).data("page") > 1) {
                            $("#pagination button").first().css("visibility", "visible");
                        }else{
                            $("#pagination button").first().css("visibility", "hidden");
                        }
                        //$('#pagination').remove();
                        //panier.modules.actions.construirePagination();
                        panier.modules.actions.TrierProduit();
                    });
                });
            }
        }
    })();
  
    window.addEventListener("load", panier.modules.app.start)
})();